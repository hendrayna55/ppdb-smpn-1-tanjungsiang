@extends('layouts.app')

@section('title')
    Tahap 6
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 6 : Pas Photo</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 6 <i class="fas fa-angle-right"></i> Pas Photo
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Pas Photo Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-6/' . $user->id)}}" method="post">
                                @csrf
                                @method('put')

                                <div class="card-body text-sm">
                                    <div class="form-group row">
                                        <label for="foto_siswa" class="col-sm-12 col-lg-3 col-form-label">Pas Photo</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->foto_siswa == null)
                                                <img src="{{asset('assets/dist/img/pas-photo/pas-photo-kosong.jpg')}}" alt="Foto User" class="d-flex justify-content-center align-items-center" width="150px">
                                            @else
                                                <img src="{{asset('assets/dist/img/pas-photo/' . $user->biodata->foto_siswa)}}" alt="Foto User" class="d-flex justify-content-center align-items-center" width="150px">
                                            @endif

                                            {{-- <input type="file" class="form-control-file @error ('foto_siswa') is-invalid @enderror" id="foto_siswa" name="foto_siswa" placeholder="Masukkan Nama Asal Sekolah" value="{{old('foto_siswa')?old('foto_siswa'):$user->biodata->foto_siswa}}" readonly> --}}

                                            @error('foto_siswa')
                                                <b class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}

                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                    <a href="{{url('/tahap-6/edit/' . $user->id)}}">
                                        <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                    </a>
                                    @endif
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection