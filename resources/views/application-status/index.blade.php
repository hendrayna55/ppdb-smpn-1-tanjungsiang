@extends('layouts.app')

@section('title')
    Konfigurasi Status
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-6">
                        <h1 class="m-0">Status Aplikasi</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-12 col-lg-6">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                            <li class="breadcrumb-item active"><i class="fas fa-cog"></i> Settings </li>
                            <li class="breadcrumb-item active"><i class="fas fa-wrench"></i> Status Aplikasi</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-6 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Data Status Aplikasi</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal">
                                <div class="card-body text-sm">

                                    {{-- <div class="form-group row">
                                        <label class="col-sm-3 col-lg-4 col-form-label">Status Pendaftaran KTA</label>
                                        <div class="col-sm-9 col-lg-7">
                                            <input type="text" class="form-control" value="{{$statusJadwalKta->statusJadwal->nama}}" readonly>
                                        </div>
                                    </div> --}}

                                    @foreach ($giat as $item)
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-md-6 col-lg-6 col-form-label">{{$item->nama_agenda}}</label>
                                            <div class="col-sm-3 col-md-3 col-lg-3">
                                                <input type="text" class="form-control" value="{{$item->status->nama}}" readonly>
                                            </div>
                                            <a href="{{url('/status-aplikasi/edit/'. $item->id)}}" class="btn btn-sm btn-success mt-1"><i class="fas fa-edit"></i></a>
                                        </div>
                                    @endforeach

                                </div>
                                <!-- /.card-body -->
                                {{-- <div class="card-footer text-sm">
                                    <a href="{{url('/status-aplikasi/edit/' . $statusJadwalKta->id)}}">
                                        <button type="button" class="btn btn-success float-right"><i class="fas fa-pen"></i> Edit</button>
                                    </a>
                                </div> --}}
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
@endsection