<?php

namespace App\Http\Controllers\DataNilai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\Models\User;
use App\Models\NilaiBahasaIndonesia;
use App\Models\NilaiMatematika;
use App\Models\NilaiIpa;
use Auth;

class DataNilaiController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        $avgIndo = ($user->nilaiIndo->kelas4_1 + $user->nilaiIndo->kelas4_2 + $user->nilaiIndo->kelas5_1 + $user->nilaiIndo->kelas5_2 + $user->nilaiIndo->kelas6_1)/5;
        $avgMtk = ($user->nilaiMtk->kelas4_1 + $user->nilaiMtk->kelas4_2 + $user->nilaiMtk->kelas5_1 + $user->nilaiMtk->kelas5_2 + $user->nilaiMtk->kelas6_1)/5;
        $avgIpa = ($user->nilaiIpa->kelas4_1 + $user->nilaiIpa->kelas4_2 + $user->nilaiIpa->kelas5_1 + $user->nilaiIpa->kelas5_2 + $user->nilaiIpa->kelas6_1)/5;

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-5.index', compact('user', 'avgIndo', 'avgMtk', 'avgIpa'));
        }else{
            abort(403);
        }
    }

    public function editBI($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-5.bahasa-indonesia.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    public function updateBI(Request $request, $id)
    {
        $request->validate([
            'kelas4_1' => ['required'],
            'kelas4_2' => ['required'],
            'kelas5_1' => ['required'],
            'kelas5_2' => ['required'],
            'kelas6_1' => ['required'],
        ]);

        $user = User::find($id);

        NilaiBahasaIndonesia::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'kelas4_1' => $request->kelas4_1,
            'kelas4_2' => $request->kelas4_2,
            'kelas5_1' => $request->kelas5_1,
            'kelas5_2' => $request->kelas5_2,
            'kelas6_1' => $request->kelas6_1,
        ]);

        alert()->success('Berhasil', 'Update Nilai Bahasa Indonesia');
        return redirect('/tahap-5/' . $user->id);
    }

    public function editMtk($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-5.matematika.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    public function updateMtk(Request $request, $id)
    {
        $request->validate([
            'kelas4_1' => ['required'],
            'kelas4_2' => ['required'],
            'kelas5_1' => ['required'],
            'kelas5_2' => ['required'],
            'kelas6_1' => ['required'],
        ]);

        $user = User::find($id);

        NilaiMatematika::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'kelas4_1' => $request->kelas4_1,
            'kelas4_2' => $request->kelas4_2,
            'kelas5_1' => $request->kelas5_1,
            'kelas5_2' => $request->kelas5_2,
            'kelas6_1' => $request->kelas6_1,
        ]);

        alert()->success('Berhasil', 'Update Nilai Matematika');
        return redirect('/tahap-5/' . $user->id);
    }

    public function editIpa($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-5.ipa.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    public function updateIpa(Request $request, $id)
    {
        $request->validate([
            'kelas4_1' => ['required'],
            'kelas4_2' => ['required'],
            'kelas5_1' => ['required'],
            'kelas5_2' => ['required'],
            'kelas6_1' => ['required'],
        ]);

        $user = User::find($id);

        NilaiIpa::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'kelas4_1' => $request->kelas4_1,
            'kelas4_2' => $request->kelas4_2,
            'kelas5_1' => $request->kelas5_1,
            'kelas5_2' => $request->kelas5_2,
            'kelas6_1' => $request->kelas6_1,
        ]);

        alert()->success('Berhasil', 'Update Nilai IPA');
        return redirect('/tahap-5/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
