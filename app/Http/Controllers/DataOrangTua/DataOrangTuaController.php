<?php

namespace App\Http\Controllers\DataOrangTua;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\Models\User;
use App\Models\DataOrangtuaSiswa;
use Auth;

class DataOrangTuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-4.index', compact('user'));
        }else{
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-4.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_ayah' => ['required'],
            'nama_ibu' => ['required'],
            'pekerjaan_ayah' => ['required'],
            'pekerjaan_ibu' => ['required'],
        ]);

        $user = User::find($id);

        DataOrangtuaSiswa::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'nama_ayah' => $request->nama_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'nama_ibu' => $request->nama_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
        ]);

        alert()->success('Berhasil', 'Update Tahap 4 : Data Orangtua');
        return redirect('/tahap-4/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
