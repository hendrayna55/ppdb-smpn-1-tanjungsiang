<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PpdbStatus extends Model
{
    protected $table = 'ppdb_statuses';
    protected $guarded = ['id'];

    public function status()
    {
        return $this->belongsTo(StatusJadwal::class, 'status_jadwals_id');
    }
}
