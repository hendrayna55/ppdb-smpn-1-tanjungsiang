<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BiodataSiswa;
use App\Models\VerifikasiPendaftaran;
use App\Models\InformasiDashboard;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = date('d M y', strtotime(Carbon::now()));
        $jumlahAdmin = User::where('role_id', 1)->count();
        $pendaftar = User::where('role_id', 2)->get();
        $totalPendaftar = User::where('role_id', 2)->count();
        $pendaftarPutra = BiodataSiswa::where('jenis_kelamin', 'Laki-Laki')->count() - $jumlahAdmin;
        $pendaftarPutri = BiodataSiswa::where('jenis_kelamin', 'Perempuan')->count();
        $waitingVerif = User::where('role_id', 2)->where('status_verifikasi', 0)->count();
        $verifDataDone = VerifikasiPendaftaran::where('verifikasi_pendaftaran', 1)->count() - $jumlahAdmin;
        $verifDataUndone = VerifikasiPendaftaran::where('verifikasi_pendaftaran', 0)->count();
        $newUser = User::orderBy('created_at', 'desc')->where('role_id', 2)->limit(8)->get();
        $allUser = User::orderBy('nama_lengkap', 'asc')->where('role_id', 2)->where('status_verifikasi', 1)->get();
        $informasi = InformasiDashboard::first();

        return view('dashboard.index', compact('totalPendaftar', 'pendaftarPutra', 'pendaftarPutri', 'waitingVerif', 'verifDataDone', 'verifDataUndone', 'newUser', 'today', 'allUser', 'informasi'));
    }
}
