<?php

namespace App\Http\Controllers\VerifikasiData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\Models\User;
use App\Models\VerifikasiPendaftaran;
use Auth;

class VerifikasiDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-7.index', compact('user'));
        }else{
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        VerifikasiPendaftaran::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'verifikasi_pendaftaran' => 1,
        ]);

        alert()->success('Berhasil', 'Verifikasi Data ' . $user->nama_lengkap);
        return redirect('/tahap-7/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
