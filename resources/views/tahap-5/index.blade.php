@extends('layouts.app')

@section('title')
    Tahap 5
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 5 : Data Nilai</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 5 <i class="fas fa-angle-right"></i> Data Nilai
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-12 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Data Riwayat Nilai Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                                <div class="card-body text-sm">

                                    <div class="row">
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="card card-success">
                                                <div class="card-header">
                                                    <h3 class="card-title">Data Nilai Bahasa Indonesia</h3>
                                                </div>
                                                <!-- /.card-header -->
                    
                                                <div class="card-body text-sm">
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_4" class=""><b>Kelas 4</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas4_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIndo->kelas4_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="{{old('kelas4_1')?old('kelas4_1'):$user->nilaiIndo->kelas4_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas4_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiIndo->kelas4_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="{{old('kelas4_2')?old('kelas4_2'):$user->nilaiIndo->kelas4_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_5" class=""><b>Kelas 5</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas5_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIndo->kelas5_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="{{old('kelas5_1')?old('kelas5_1'):$user->nilaiIndo->kelas5_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas5_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiIndo->kelas5_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="{{old('kelas5_2')?old('kelas5_2'):$user->nilaiIndo->kelas5_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_6" class=""><b>Kelas 6</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas6_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIndo->kelas6_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="{{old('kelas6_1')?old('kelas6_1'):$user->nilaiIndo->kelas6_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas6_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                
                                                <div class="card-footer text-sm">
                                                    
                                                    <b>Rata-rata : {{$avgIndo}}</b>

                                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                                        <a href="{{url('/tahap-5/bahasa-indonesia/edit/' . $user->id)}}">
                                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                                        </a>
                                                    @endif
                                                </div>
                                                <!-- /.card-footer -->
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="card card-success">
                                                <div class="card-header">
                                                    <h3 class="card-title">Data Nilai Matematika</h3>
                                                </div>
                                                <!-- /.card-header -->
                    
                                                <div class="card-body text-sm">
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_4" class=""><b>Kelas 4</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas4_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiMtk->kelas4_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="{{old('kelas4_1')?old('kelas4_1'):$user->nilaiMtk->kelas4_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas4_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiMtk->kelas4_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="{{old('kelas4_2')?old('kelas4_2'):$user->nilaiMtk->kelas4_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_5" class=""><b>Kelas 5</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas5_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiMtk->kelas5_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="{{old('kelas5_1')?old('kelas5_1'):$user->nilaiMtk->kelas5_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas5_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiMtk->kelas5_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="{{old('kelas5_2')?old('kelas5_2'):$user->nilaiMtk->kelas5_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_6" class=""><b>Kelas 6</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas6_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiMtk->kelas6_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="{{old('kelas6_1')?old('kelas6_1'):$user->nilaiMtk->kelas6_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas6_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                
                                                <div class="card-footer text-sm">
                                                    
                                                    <b>Rata-rata : {{$avgMtk}}</b>  
                
                                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                                        <a href="{{url('/tahap-5/nilai-matematika/edit/' . $user->id)}}">
                                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                                        </a>
                                                    @endif
                                                </div>
                                                <!-- /.card-footer -->
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-sm-12">
                                            <div class="card card-success">
                                                <div class="card-header">
                                                    <h3 class="card-title">Data Nilai IPA</h3>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body text-sm">
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_4" class=""><b>Kelas 4</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas4_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIpa->kelas4_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="{{old('kelas4_1')?old('kelas4_1'):$user->nilaiIpa->kelas4_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas4_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiIpa->kelas4_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="{{old('kelas4_2')?old('kelas4_2'):$user->nilaiIpa->kelas4_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas4_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_5" class=""><b>Kelas 5</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas5_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIpa->kelas5_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="{{old('kelas5_1')?old('kelas5_1'):$user->nilaiIpa->kelas5_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas5_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                                <div class="col-6 col-lg-7">
                                                                    @if ($user->nilaiIpa->kelas5_2 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="{{old('kelas5_2')?old('kelas5_2'):$user->nilaiIpa->kelas5_2}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas5_2')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center mx-auto align-items-center">
                                                        <div class="col-lg-3 col-sm-12 text-center">
                                                            <label for="kelas_6" class=""><b>Kelas 6</b></label>
                                                        </div>
                                                        <div class="col-lg-9 col-sm-12">
                                                            <div class="form-group row">
                                                                <label for="kelas6_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                                <div class="col-lg-7 col-6">
                                                                    @if ($user->nilaiIpa->kelas6_1 == null)
                                                                        <input type="text" class="form-control text-danger @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="Belum Update" readonly>
                                                                    @else
                                                                        <input type="text" class="form-control @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="{{old('kelas6_1')?old('kelas6_1'):$user->nilaiIpa->kelas6_1}}" readonly>
                                                                    @endif
                        
                                                                    @error('kelas6_1')
                                                                        <p class="text-danger text-sm">{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                
                                                <div class="card-footer text-sm">
                                                    
                                                    <b>Rata-rata : {{$avgIpa}}</b>   
                
                                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                                        <a href="{{url('/tahap-5/nilai-ipa/edit/' . $user->id)}}">
                                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                                        </a>
                                                    @endif
                                                </div>
                                                <!-- /.card-footer -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}   

                                    {{-- <a href="{{url('/tahap-4/edit/' . $user->id)}}">
                                        <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                    </a> --}}
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection