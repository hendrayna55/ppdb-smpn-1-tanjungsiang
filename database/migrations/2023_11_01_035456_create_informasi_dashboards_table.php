<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformasiDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informasi_dashboards', function (Blueprint $table) {
            $table->id();
            $table->string('foto_informasi');
            $table->text('deskripsi_informasi');
            $table->timestamps();
        });

        DB::table('informasi_dashboards')->insert([
            'foto_informasi' => 'gambar_dashboard.jpeg', 
            'deskripsi_informasi' => '<h5>Selamat Datang, Kak Hendra Ahmadillah</h5><p>Ini adalah dashboard dari Aplikasi <span><b>Cilaku Scout Data Management System (CSDMS)</b></span>. Kakak bisa menikmati semua fitur yang ada ya. Selamat beraktivitas.</p><p>Lengkapi data yang ada di aplikasi ini agar kami dari Kwartir Ranting dan Dewan Kerja Ranting bisa lebih optimal dalam melaksanakan pendataan informasi di lingkungan Kwartir ya.</p>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informasi_dashboards');
    }
}
