@extends('layouts.app')

@section('title')
    Pendaftar
@endsection

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@push('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": false, "lengthChange": false, "autoWidth": false, "ordering": true,
                "buttons": ["csv", "excel", "pdf", "print",] , "scrollX" : true
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pendaftar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><i class="fas fa-users"></i> Pendaftar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-info">
                <h3 class="card-title">Data Pendaftar PPDB SMPN 1 Tanjungsiang</h3>
                <a href="{{url('/pendaftar/tambah')}}">
                  <button class="btn btn-sm btn-success float-right shadow">Tambah</button>
                </a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-sm text-sm nowrap">
                  <thead class="bg-info">
                    <tr>
                      <th class="text-center align-middle">Pas Photo</th>
                      <th class="text-center align-middle">Nama Lengkap</th>
                      <th class="text-center align-middle">Status Akun</th>
                      <th class="text-center align-middle">Jenis Kelamin</th>
                      <th class="text-center align-middle">Asal Sekolah</th>
                      <th class="text-center align-middle">Nomor HP</th>
                      <th class="text-center align-middle">Verifikasi Data</th>
                      <th class="text-center align-middle">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $item)
                      <tr>
                        @if ($item->biodata->foto_siswa == null)
                            <td class="text-center align-middle">
                              <a href="{{url('/tahap-6/' . $item->id)}}"><img src="{{asset('assets/dist/img/pas-photo/pas-photo-kosong.jpg')}}" alt="Pas Photo Siswa" width="110px"></a>
                            </td>
                        @else
                          <td class="text-center align-middle">
                            <a href="{{asset('assets/dist/img/pas-photo/' . $item->biodata->foto_siswa)}}" target="_blank"><img src="{{asset('assets/dist/img/pas-photo/' . $item->biodata->foto_siswa)}}" alt="Pas Photo Siswa" width="110px"></a>
                          </td>
                        @endif
                        <td class="text-center align-middle">{{$item->nama_lengkap}}</td>

                        @if ($item->status_verifikasi == 0)
                          @if ($item->biodata->tempat_lahir == null || $item->biodata->tanggal_lahir == null || $item->biodata->nisn == null)
                            <td class="text-center align-middle">
                              <div class="btn btn-sm text-danger" data-toggle="modal" data-target="#modal-verifikasi{{$item->id}}">Belum Isi Data Lanjutan</div>
                            </td>
                          @else
                            <td class="text-center align-middle">
                              <div class="btn btn-sm text-danger" data-toggle="modal" data-target="#modal-verifikasi{{$item->id}}">Menunggu Verifikasi</div>
                            </td>
                          @endif
                        @else
                          <td class="text-center align-middle">
                            <div class="btn btn-sm text-success" data-toggle="modal" data-target="#modal-verifikasi{{$item->id}}">Terverifikasi</div>
                          </td>
                        @endif

                        <td class="text-center align-middle">{{$item->biodata->jenis_kelamin}}</td>
                        <td class="text-center align-middle">{{$item->asalSekolah->asal_sekolah}}</td>
                        <td class="text-center align-middle">{{$item->biodata->no_hp}}</td>

                        @if ($item->verifikasi->verifikasi_pendaftaran == 0)
                          <td class="text-center align-middle">
                            <div class="btn btn-sm text-danger" data-toggle="modal" data-target="#modal-verifikasi-data{{$item->id}}">Belum Verifikasi Data</div>
                          </td>
                        @else
                          <td class="text-center align-middle">
                            <div class="btn btn-sm text-success" data-toggle="modal" data-target="#modal-verifikasi-data{{$item->id}}">Sudah Verifikasi Data</div>
                          </td>
                        @endif

                        <td class="text-center align-middle">
                          <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-lihat{{$item->id}}" type="button"><i class="fas fa-eye"></i></button>
                          <a href="{{url('/pendaftar/edit/' . $item->id)}}"><button class="btn btn-sm btn-success mx-1"><i class="fas fa-edit"></i></button></a>
                          <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-hapus{{$item->id}}"><i class="fas fa-trash-alt"></i></button>
                        </td>
                      </tr>

                      <!-- Modal Lihat -->
                      <div class="modal fade" id="modal-lihat{{$item->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header bg-info">
                              <h4 class="modal-title">Profile Pendaftar</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12 col-lg-4 col-sm-12 mb-3 text-center">
                                  @if ($item->biodata->foto_siswa == null)
                                      <a href="{{url('/tahap-6/' . $item->id)}}"><img src="{{asset('assets/dist/img/pas-photo/pas-photo-kosong.jpg')}}" alt="Pas Photo" width="200px" class=""></a><br>
                                      <small><b class="text-danger text-center"><i>**Belum Update Pas Photo</i></b></small>
                                  @else
                                    <a href="{{url('/tahap-6/' . $item->id)}}"><img src="{{asset('assets/dist/img/pas-photo/' . $item->biodata->foto_siswa)}}" alt="Pas Photo" width="200px"></a>
                                  @endif
                                </div>

                                <div class="col-12 col-lg-8 col-sm-12">
                                  <label for=""><a href="{{url('/tahap-1/' . $item->id)}}">Biodata</a></label><br>
                                  <div class="row text-sm">
                                    <div class="col-4 col-lg-4 col-sm-3">
                                      Nama Lengkap<br>
                                      Jenis Kelamin<br>
                                      Nomor HP<br>
                                      Email<br>
                                      Tmpt, Tgl Lahir<br>
                                      NISN<br>
                                      NIK<br>
                                    </div>
                                    <div class="col-8 col-lg-8 col-sm-9">
                                      : {{$item->nama_lengkap}}<br>
                                      : {{$item->biodata->jenis_kelamin}}<br>
                                      : {{$item->biodata->no_hp}}<br>
                                      : {{$item->email}}<br>

                                      @if ($item->biodata->tempat_lahir == null || $item->biodata->tanggal_lahir == null)
                                        : <span class="text-danger">Belum Update Data Lanjutan</span><br>
                                      @else
                                        : {{$item->biodata->tempat_lahir}}, {{date('d M Y', strtotime($item->biodata->tanggal_lahir))}}<br>
                                      @endif

                                      @if ($item->biodata->nisn == null)
                                        : <span class="text-danger">Belum Update Data Lanjutan</span><br>
                                      @else
                                        : {{$item->biodata->nisn}}<br>
                                      @endif

                                      @if ($item->biodata->nik_siswa == null)
                                        : <span class="text-danger">Belum Update Tahap 1</span><br><br>
                                      @else
                                        : {{$item->biodata->nik_siswa}}<br><br>
                                      @endif
                                      
                                    </div>
                                  </div>

                                  <label for=""><a href="{{url('/tahap-2/' . $item->id)}}">Alamat</a></label><br>
                                  <div class="row text-sm">
                                    <div class="col-12 col-lg-12 col-sm-12">
                                      @if ($item->biodata->alamat == null)
                                        <span class="text-danger">Belum Update Tahap 2</span><br><br>
                                      @else
                                        {{$item->biodata->alamat}}<br><br>
                                      @endif
                                    </div>
                                  </div>

                                  <label for=""><a href="{{url('/tahap-3/' . $item->id)}}">Riwayat Pendidikan</a></label><br>
                                  <div class="row text-sm">
                                    <div class="col-4 col-lg-4 col-sm-4">
                                      Asal Sekolah<br>
                                      Tahun Lulus<br>
                                    </div>
                                    <div class="col-8 col-lg-8 col-sm-8">
                                      : {{$item->asalSekolah->asal_sekolah}}<br>

                                      @if ($item->asalSekolah->tahun_lulus == null)
                                        : <span class="text-danger">Belum Update Tahap 3</span><br><br>
                                      @else
                                        : {{$item->asalSekolah->tahun_lulus}}<br><br>
                                      @endif
                                    </div>
                                  </div>

                                  <label for=""><a href="{{url('/tahap-4/' . $item->id)}}">Data Orang Tua</a></label><br>
                                  <div class="row text-sm">
                                    <div class="col-4 col-lg-4 col-sm-5">
                                      Nama Ayah<br>
                                      Pekerjaan Ayah<br>
                                      Nama Ibu<br>
                                      Pekerjaan Ibu<br>
                                    </div>
                                    <div class="col-8 col-lg-8 col-sm-7">
                                      @if ($item->orangtua->nama_ayah == null)
                                        : <span class="text-danger">Belum Update Tahap 4</span><br>
                                      @else
                                        : {{$item->orangtua->nama_ayah}}<br>
                                      @endif
                                      
                                      @if ($item->orangtua->pekerjaan_ayah == null)
                                        : <span class="text-danger">Belum Update Tahap 4</span><br>
                                      @else
                                        : {{$item->orangtua->pekerjaan_ayah}}<br>
                                      @endif

                                      @if ($item->orangtua->nama_ibu == null)
                                        : <span class="text-danger">Belum Update Tahap 4</span><br>
                                      @else
                                        : {{$item->orangtua->nama_ibu}}<br>
                                      @endif
                                      
                                      @if ($item->orangtua->pekerjaan_ibu == null)
                                        : <span class="text-danger">Belum Update Tahap 4</span><br><br>
                                      @else
                                        : {{$item->orangtua->pekerjaan_ibu}}<br><br>
                                      @endif
                                    </div>
                                  </div>

                                  <label for=""><a href="{{url('/tahap-5/' . $item->id)}}">Data Rata-Rata Nilai</a></label><br>
                                  <div class="row text-sm">
                                    <div class="col-4 col-lg-4 col-sm-5">
                                      Bahasa Indonesia<br>
                                      Matematika<br>
                                      IPA<br>
                                    </div>
                                    <div class="col-8 col-lg-8 col-sm-7">
                                      @php
                                          $avgIndo = ($item->nilaiIndo->kelas4_1 + $item->nilaiIndo->kelas4_2 + $item->nilaiIndo->kelas5_1 + $item->nilaiIndo->kelas5_2 + $item->nilaiIndo->kelas6_1)/5;

                                          $avgMtk = ($item->nilaiMtk->kelas4_1 + $item->nilaiMtk->kelas4_2 + $item->nilaiMtk->kelas5_1 + $item->nilaiMtk->kelas5_2 + $item->nilaiMtk->kelas6_1)/5;

                                          $avgIpa = ($item->nilaiIpa->kelas4_1 + $item->nilaiIpa->kelas4_2 + $item->nilaiIpa->kelas5_1 + $item->nilaiIpa->kelas5_2 + $item->nilaiIpa->kelas6_1)/5;
                                      @endphp

                                      : <b>{{$avgIndo}}</b><br>
                                      
                                      : <b>{{$avgMtk}}</b><br>

                                      : <b>{{$avgIpa}}</b><br>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->

                      <!-- Modal Verifikasi Data -->
                      <div class="modal fade" id="modal-verifikasi-data{{$item->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="{{url('/pendaftar/verifikasi-data/' . $item->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="modal-header bg-warning text-center">
                                        <h4 class="modal-title">Verifikasi Pendaftar</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-center">
                                            <div class="row justify-content-center text-sm">
                                                <div class="col-4 col-lg-3">
                                                    Nama Lengkap<br>
                                                    Jenis Kelamin<br>
                                                    Asal Sekolah<br>

                                                </div>
                                                <div class="col-8 col-lg-8">
                                                    : {{$item->nama_lengkap}}<br>
                                                    : {{$item->biodata->jenis_kelamin}}<br>
                                                    : {{$item->asalSekolah->asal_sekolah}}<br>

                                                </div>
                                            </div>

                                            <div class="row text-center">
                                                <div class="col-12">
                                                    <label for="verifikasi_pendaftaran" class="col-form-label" style="margin-bottom: -100px; margin-top: 25px">Status Verifikasi</label>
                                                    <select name="verifikasi_pendaftaran" id="" class="mt-2 form-control @error('verifikasi_pendaftaran') is-invalid @enderror">    
                                                        <option value="1" <?php if($item->verifikasi->verifikasi_pendaftaran == 1) echo 'selected' ?>>
                                                            Sudah Verifikasi
                                                        </option>
                                                        <option value="0" <?php if($item->verifikasi->verifikasi_pendaftaran == 0) echo 'selected' ?>>
                                                            Belum Verifikasi
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="d-none">
                                                <input name="nama" value="{{$item->nama}}">
                                            </div>
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-info float-right" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-success float-right">Simpan</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->

                      <!-- Modal Verifikasi Akun -->
                      <div class="modal fade" id="modal-verifikasi{{$item->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="{{url('/pendaftar/verifikasi/' . $item->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="modal-header bg-success text-center">
                                        <h4 class="modal-title">Verifikasi Pendaftar</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-center">
                                            <div class="row justify-content-center text-sm">
                                                <div class="col-4 col-lg-3">
                                                    Nama Lengkap<br>
                                                    Jenis Kelamin<br>
                                                    Asal Sekolah<br>

                                                </div>
                                                <div class="col-8 col-lg-8">
                                                    : {{$item->nama_lengkap}}<br>
                                                    : {{$item->biodata->jenis_kelamin}}<br>
                                                    : {{$item->asalSekolah->asal_sekolah}}<br>

                                                </div>
                                            </div>

                                            <div class="row text-center">
                                                <div class="col-12">
                                                    <label for="status_verifikasi" class="col-form-label" style="margin-bottom: -100px; margin-top: 25px">Status Verifikasi</label>
                                                    <select name="status_verifikasi" id="" class="mt-2 form-control @error('status_verifikasi') is-invalid @enderror">    
                                                        <option value="1" <?php if($item->status_verifikasi == 1) echo 'selected' ?>>
                                                            Terverifikasi
                                                        </option>
                                                        <option value="0" <?php if($item->status_verifikasi == 0) echo 'selected' ?>>
                                                            Belum Verifikasi
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="d-none">
                                                <input name="nama" value="{{$item->nama}}">
                                            </div>
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-info float-right" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-success float-right">Simpan</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->

                      <!-- Modal Hapus -->
                      <div class="modal fade" id="modal-hapus{{$item->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h4 class="modal-title">Hapus Akun Pendaftar</h4>
                                    <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button> -->
                                </div>
                                <div class="modal-body">
                                  <div class="row justify-content-center text-sm">
                                      <div class="col-4 col-sm-4 col-md-3">
                                          Nama Lengkap<br>
                                          Jenis Kelamin<br>
                                          Asal Sekolah<br>

                                      </div>
                                      <div class="col-8 col-sm-8 col-md-8">
                                          : {{$item->nama_lengkap}}<br>
                                          : {{$item->biodata->jenis_kelamin}}<br>
                                          : {{$item->asalSekolah->asal_sekolah}}<br>

                                      </div>
                                  </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    <form action="{{url('/pendaftar/' . $item->id)}}" method="post" class="">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-hapus">Hapus</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                    @endforeach
                  
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection