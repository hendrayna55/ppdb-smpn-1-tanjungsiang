<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiMatematika extends Model
{
    protected $table = 'nilai_matematikas';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
