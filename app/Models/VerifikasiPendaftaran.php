<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifikasiPendaftaran extends Model
{
    protected $table = 'verifikasi_pendaftarans';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
