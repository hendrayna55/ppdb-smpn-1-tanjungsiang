<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @php
        $aplikasi = App\Models\ApplicationName::first();
    @endphp
    <title>Register Tahap 2 PPDB</title>
    <link rel="icon" href="{{asset('assets/dist/img/' . $aplikasi->application_logo)}}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets')}}/dist/css/adminlte.min.css">
</head>
<body class="hold-transition register-page">

  <div class="register-box">
    <div class="register-logo">
      <img src="{{asset('assets/dist/img/' . $aplikasi->application_logo)}}" alt="Logo Sekolah" width="100px"><br>
      <a href="{{url('/register')}}"><b>{{$aplikasi->application_nickname}}</b> Online</a>
    </div>

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Lengkapi Data Di Bawah Ini</p>

        <form action="{{url('/registrasi/lengkapi-data/' . $user->id)}}" method="post">
          @csrf
          @method('put')

          <div class="input-group mb-3">
            <input id="nama_lengkap" type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ old('nama_lengkap')?old('nama_lengkap'):$user->nama_lengkap }}" readonly autocomplete="nama_lengkap" placeholder="Tempat Lahir Siswa">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>

            @error('nama_lengkap')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="input-group mb-3">
            <input id="tempat_lahir" type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ old('tempat_lahir')?old('tempat_lahir'):$user->biodata->tempat_lahir }}"  autocomplete="tempat_lahir" placeholder="Tempat Lahir Siswa">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-map-marker-alt"></span>
              </div>
            </div>

            @error('tempat_lahir')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="input-group mb-3">
            <input id="tanggal_lahir" type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir')?old('tanggal_lahir'):$user->biodata->tanggal_lahir }}"  autocomplete="tanggal_lahir" placeholder="Tanggal Lahir Siswa">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-calendar-alt"></span>
              </div>
            </div>

            @error('tanggal_lahir')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="input-group mb-3">
            <input id="nisn" type="text" class="form-control @error('nisn') is-invalid @enderror" name="nisn" value="{{ old('nisn')?old('nisn'):$user->biodata->nisn }}"  autocomplete="nisn" placeholder="NISN Siswa">

            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-id-card"></span>
              </div>
            </div>

            @error('nisn')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="row">
            <div class="col-8">
              {{-- <div class="icheck-primary">
                <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                <label for="agreeTerms">
                I agree to the <a href="#">terms</a>
                </label>
              </div> --}}
              <div class="col-6">
                
                <button type="button" onclick="logoutform();" class="btn btn-danger btn-block">
                  Logout
                </button>

              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
        </form>

        {{-- <a href="{{url('/login')}}" class="text-center">Saya sudah punya akun</a>   --}}
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
  <!-- /.register-box -->

  <!-- jQuery -->
  <script src="{{asset('assets')}}/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="{{asset('assets')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('assets')}}/dist/js/adminlte.min.js"></script>
  <script>
    function logoutform(event)
    {
      document.getElementById('logout-form').submit();
    }
  </script>
  @include('sweetalert::alert')
</body>
</html>