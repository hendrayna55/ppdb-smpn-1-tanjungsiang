<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\pangkalan\PangkalanController;
use App\Http\Controllers\Auth\RegisterManualController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Biodata\BiodataController;
use App\Http\Controllers\RiwayatPendidikan\RiwayatPendidikanController;
use App\Http\Controllers\DataOrangTua\DataOrangTuaController;
use App\Http\Controllers\DataNilai\DataNilaiController;
use App\Http\Controllers\FotoSiswa\FotoSiswaController;
use App\Http\Controllers\Pendaftar\PendaftarController;
use App\Http\Controllers\VerifikasiData\VerifikasiDataController;
use App\Http\Controllers\Narahubung\NarahubungController;
use App\Http\Controllers\Akun\UbahPassword\UbahPasswordController;
use App\Http\Controllers\DataAplikasi\DataAplikasiController;
use App\Http\Controllers\StatusAplikasiController;
use App\Http\Controllers\InformasiDashboard\InformasiDashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home')->middleware('auth', 'verified_user');

Route::prefix('registrasi')->group(function() 
{
    Route::get('/',[RegisterManualController::class,'create']);
    Route::post('/tambahAkun',[RegisterManualController::class,'store']);
    Route::get('/kelengkapan-data/{id}',[RegisterManualController::class,'tahap2']);
    Route::put('/lengkapi-data/{id}',[RegisterManualController::class,'tahapData2']);
    Route::get('/edit',[RegisterManualController::class,'ShowEditPangkalan']);
});

Auth::routes();
// Route::get('/logout',[LoginController::class,'logout']);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['superadmin'])->group(function()
{
    Route::prefix('/')->group(function() 
    {
        Route::prefix('informasi')->group(function() 
        {
            Route::get('/deskripsi/edit', [InformasiDashboardController::class, 'editDeskripsi']);
            Route::put('/deskripsi/{id}', [InformasiDashboardController::class, 'updateDeskripsi']);

            Route::get('/',[StatusAplikasiController::class,'index']);
            Route::get('/edit/{id}',[StatusAplikasiController::class,'edit']);
            Route::put('/{id}',[StatusAplikasiController::class,'update']);
        });

        Route::prefix('pendaftar')->group(function() 
        {
            Route::get('/',[PendaftarController::class,'index']);
            Route::put('/verifikasi-data/{id}',[PendaftarController::class,'verifikasiData']);
            Route::put('/verifikasi/{id}',[PendaftarController::class,'verifikasi']);
            Route::get('/tambah',[PendaftarController::class,'create']);
            Route::post('/',[PendaftarController::class,'store']);
            Route::get('/edit/{id}',[PendaftarController::class,'edit']);
            Route::put('/{id}',[PendaftarController::class,'update']);
            Route::delete('/{id}',[PendaftarController::class,'destroy']);
        });

        Route::prefix('contact-person')->group(function()
        {
            Route::get('/create',[NarahubungController::class,'create']);
            Route::post('/', [NarahubungController::class, 'store']);
            Route::get('/edit/{id}',[NarahubungController::class,'edit']);
            Route::put('/{id}',[NarahubungController::class,'update']);
            Route::delete('/{id}',[NarahubungController::class,'destroy']);
        });

        Route::prefix('data-aplikasi')->group(function() 
        {
            Route::get('/',[DataAplikasiController::class,'index']);
            Route::get('/edit/{id}',[DataAplikasiController::class,'edit']);
            Route::put('/{id}',[DataAplikasiController::class,'update']);
        });

        Route::prefix('status-aplikasi')->group(function() 
        {
            Route::get('/',[StatusAplikasiController::class,'index']);
            Route::get('/edit/{id}',[StatusAplikasiController::class,'edit']);
            Route::put('/{id}',[StatusAplikasiController::class,'update']);
        });
    });
});

Route::middleware(['auth', 'verified_user'])->group(function()
{
    Route::prefix('/')->group(function() 
    {
        Route::get('/dashboard',[HomeController::class,'index']);

        Route::prefix('tahap-1')->group(function() 
        {
            Route::get('/{id}',[BiodataController::class,'indexBio']);
            Route::get('/edit/{id}',[BiodataController::class,'editBio']);
            Route::put('/{id}',[BiodataController::class,'updateBio']);
        });

        Route::prefix('tahap-2')->group(function() 
        {
            Route::get('/{id}',[BiodataController::class,'indexAlamat']);
            Route::get('/edit/{id}',[BiodataController::class,'editAlamat']);
            Route::put('/{id}',[BiodataController::class,'updateAlamat']);
        });

        Route::prefix('tahap-3')->group(function() 
        {
            Route::get('/{id}',[RiwayatPendidikanController::class,'index']);
            Route::get('/edit/{id}',[RiwayatPendidikanController::class,'edit']);
            Route::put('/{id}',[RiwayatPendidikanController::class,'update']);
        });

        Route::prefix('tahap-4')->group(function() 
        {
            Route::get('/{id}',[DataOrangTuaController::class,'index']);
            Route::get('/edit/{id}',[DataOrangTuaController::class,'edit']);
            Route::put('/{id}',[DataOrangTuaController::class,'update']);
        });

        Route::prefix('tahap-5')->group(function() 
        {
            Route::get('/{id}',[DataNilaiController::class,'index']);

            Route::prefix('bahasa-indonesia')->group(function() 
            {
                Route::get('/edit/{id}',[DataNilaiController::class,'editBI']);
                Route::put('/{id}',[DataNilaiController::class,'updateBI']);
            });

            Route::prefix('nilai-matematika')->group(function() 
            {
                Route::get('/edit/{id}',[DataNilaiController::class,'editMtk']);
                Route::put('/{id}',[DataNilaiController::class,'updateMtk']);
            });

            Route::prefix('nilai-ipa')->group(function() 
            {
                Route::get('/edit/{id}',[DataNilaiController::class,'editIpa']);
                Route::put('/{id}',[DataNilaiController::class,'updateIpa']);
            });
        });

        Route::prefix('tahap-6')->group(function() 
        {
            Route::get('/{id}',[FotoSiswaController::class,'index']);
            Route::get('/edit/{id}',[FotoSiswaController::class,'edit']);
            Route::put('/{id}',[FotoSiswaController::class,'update']);
        });

        Route::prefix('tahap-7')->group(function() 
        {
            Route::get('/{id}',[VerifikasiDataController::class,'index']);
            Route::get('/edit/{id}',[VerifikasiDataController::class,'edit']);
            Route::put('/{id}',[VerifikasiDataController::class,'update']);
        });

        Route::prefix('contact-person')->group(function()
        {
            Route::get('/',[NarahubungController::class,'index']);
        });

        Route::prefix('akun')->group(function() 
        {
            Route::get('/ubah-password', 'UbahPasswordController@index');

            Route::post('/ubah-password', 'UbahPasswordController@store')->name('change.password');
            // Route::post('/ubah-password',[UbahPasswordController::class,'store']);
        });
    });
});