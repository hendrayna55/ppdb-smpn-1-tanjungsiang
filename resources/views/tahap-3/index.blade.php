@extends('layouts.app')

@section('title')
    Tahap 3
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 3 : Riwayat Pendidikan</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 3 <i class="fas fa-angle-right"></i> Riwayat Pendidikan
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Riwayat Pendidikan Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-3/' . $user->id)}}" method="post">
                                @csrf
                                @method('put')

                                <div class="card-body text-sm">
                                    <div class="form-group row">
                                        <label for="asal_sekolah" class="col-sm-12 col-lg-3 col-form-label">Asal Sekolah</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('asal_sekolah') is-invalid @enderror" id="asal_sekolah" name="asal_sekolah" placeholder="Masukkan Nama Asal Sekolah" value="{{old('asal_sekolah')?old('asal_sekolah'):$user->asalSekolah->asal_sekolah}}" readonly>

                                            @error('asal_sekolah')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tahun_lulus" class="col-sm-12 col-lg-3 col-form-label">Tahun Lulus</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->asalSekolah->tahun_lulus == null)
                                                <input type="text" class="form-control text-danger @error ('tahun_lulus') is-invalid @enderror" id="tahun_lulus" name="tahun_lulus" placeholder="Masukkan Tahun Lulus" value="Belum Update" readonly>
                                            @else
                                                <input type="number" class="form-control @error ('tahun_lulus') is-invalid @enderror" id="tahun_lulus" name="tahun_lulus" placeholder="Masukkan Tahun Lulus" value="{{old('tahun_lulus')?old('tahun_lulus'):$user->asalSekolah->tahun_lulus}}" readonly>
                                            @endif

                                            @error('tahun_lulus')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}

                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                        <a href="{{url('/tahap-3/edit/' . $user->id)}}">
                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                        </a>
                                    @endif
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection