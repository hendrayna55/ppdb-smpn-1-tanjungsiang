@extends('layouts.app')

@section('title')
    Tahap 4
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 4 : Data Orangtua</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 4 <i class="fas fa-angle-right"></i> Data Orangtua
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Data Orangtua Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-4/' . $user->id)}}" method="post">
                                @csrf
                                @method('put')

                                <div class="card-body text-sm">
                                    <div class="form-group row">
                                        <label for="nama_ayah" class="col-sm-12 col-lg-3 col-form-label">Nama Ayah</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('nama_ayah') is-invalid @enderror" id="nama_ayah" name="nama_ayah" placeholder="Masukkan Nama Ayah Siswa" value="{{old('nama_ayah')?old('nama_ayah'):$user->orangtua->nama_ayah}}">

                                            @error('nama_ayah')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pekerjaan_ayah" class="col-sm-12 col-lg-3 col-form-label">Pekerjaan Ayah</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('pekerjaan_ayah') is-invalid @enderror" id="pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Masukkan Pekerjaan Ayah Siswa" value="{{old('pekerjaan_ayah')?old('pekerjaan_ayah'):$user->orangtua->pekerjaan_ayah}}">

                                            @error('pekerjaan_ayah')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nama_ibu" class="col-sm-12 col-lg-3 col-form-label">Nama Ibu</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('nama_ibu') is-invalid @enderror" id="nama_ibu" name="nama_ibu" placeholder="Masukkan Nama Ibu Siswa" value="{{old('nama_ibu')?old('nama_ibu'):$user->orangtua->nama_ibu}}">

                                            @error('nama_ibu')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pekerjaan_ibu" class="col-sm-12 col-lg-3 col-form-label">Pekerjaan Ibu</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('pekerjaan_ibu') is-invalid @enderror" id="pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Masukkan Pekerjaan Ibu Siswa" value="{{old('pekerjaan_ibu')?old('pekerjaan_ibu'):$user->orangtua->pekerjaan_ibu}}">

                                            @error('pekerjaan_ibu')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    <button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>

                                    <a href="{{url('/tahap-4/' . $user->id)}}">
                                        <button type="button" class="btn btn-info"><i class="fas fa-backward"></i> Kembali</button>
                                    </a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection