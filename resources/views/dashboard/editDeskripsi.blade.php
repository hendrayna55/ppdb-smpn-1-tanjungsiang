@extends('layouts.admin.app')

@section('title')
    Informasi
@endsection

@push('styles')
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/summernote/summernote-bs4.min.css">
@endpush

@push('scripts')
    <!-- Summernote -->
    <script src="{{asset('assets')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tentang Saya</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Tentang Saya</li>
                            <li class="breadcrumb-item">Deskripsi</li>
                            <li class="breadcrumb-item">Edit</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <form action="{{url('/admin/tentang-saya/deskripsi/' . $data->id)}}" method="post">
                            @csrf
                            @method('put')

                            <div class="card card-outline card-info">
                                <div class="card-header bg-info">
                                    <h3 class="card-title">
                                        Deskripsi Kalimat Tentang Saya
                                    </h3>
                                    <button type="submit" class="btn btn-success float-right shadow">Simpan</button>
                                    <a href="{{url('/admin/tentang-saya')}}" class="btn btn-secondary float-right shadow mr-3">Kembali</a>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
    
                                    <textarea id="summernote" name="text_kalimat_tentang_saya">{{ old('text_kalimat_tentang_saya')?old('text_kalimat_tentang_saya'):$data->text_kalimat_tentang_saya }}</textarea>
                                </div>
                                <div class="card-footer">
                                    {{-- <p>Deskripsi ini muncul di page tentang saya</p> --}}
                                    <a href="{{url('/admin/tentang-saya')}}" class="btn btn-secondary shadow">Kembali</a>
                                    <button type="submit" class="btn btn-success float-right shadow">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- ./row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection