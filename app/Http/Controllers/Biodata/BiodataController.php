<?php

namespace App\Http\Controllers\Biodata;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BiodataSiswa;
use Alert;
use Auth;

class BiodataController extends Controller
{
    public function indexBio($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-1.index', compact('user'));
        }else{
            abort(403);
        }
    }

    public function indexAlamat($id)
    {
        $user = User::find($id);

        if (Auth::user()->id == $user->id || Auth::user()->role_id == 1) {
            return view('tahap-2.index', compact('user'));
        }else{
            abort(403);
        }
    }

    public function editBio($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-1.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    public function editAlamat($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-2.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    public function updateBio(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => ['required'],
            'jenis_kelamin' => ['required'],
            'nik_siswa' => ['required'],
            'nisn' => ['required'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
        ]);

        $user = User::find($id);
        User::find($id)->update([
            'nama_lengkap' => $request->nama_lengkap,
            'role_id' => \DB::raw('role_id'),
            'email' => \DB::raw('email'),
            'status_verifikasi' => \DB::raw('status_verifikasi'),
        ]);

        BiodataSiswa::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'nik_siswa' => $request->nik_siswa,
            'nisn' => $request->nisn,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp' => \DB::raw('no_hp'),
            'alamat' => \DB::raw('alamat'),
        ]);

        alert()->success('Berhasil', 'Update Tahap 1 : Biodata');
        return redirect('/tahap-1/' . $user->id);
    }

    public function updateAlamat(Request $request, $id)
    {
        $request->validate([
            'alamat' => ['required'],
        ]);

        $user = User::find($id);

        BiodataSiswa::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'nik_siswa' => \DB::raw('nik_siswa'),
            'nisn' => \DB::raw('nisn'),
            'tempat_lahir' => \DB::raw('tempat_lahir'),
            'tanggal_lahir' => \DB::raw('tanggal_lahir'),
            'jenis_kelamin' => \DB::raw('jenis_kelamin'),
            'no_hp' => \DB::raw('no_hp'),
            'alamat' => $request->alamat,
        ]);

        alert()->success('Berhasil', 'Update Tahap 2 : Alamat');
        return redirect('/tahap-2/' . $user->id);
    }
}
