<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BiodataSiswa extends Model
{
    protected $table = 'biodata_siswas';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
