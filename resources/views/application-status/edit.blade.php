@extends('layouts.app')

@section('title')
    Edit Status {{$giat->nama_giat}}
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12 col-lg-6">
                        <h1 class="m-0">Status Aplikasi</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-12 col-lg-6">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                            <li class="breadcrumb-item active"><i class="fas fa-cog"></i> Settings </li>
                            <li class="breadcrumb-item active"><i class="fas fa-wrench"></i> Status Aplikasi <i class="fas fa-angle-right"></i> Edit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-6">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Edit Status {{$giat->nama_giat}}</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal" action="{{url('/status-aplikasi/' . $giat->id)}}" method="post">
                                @csrf
                                @method('PUT')

                                <div class="card-body text-sm">

                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-6 col-form-label">Status {{$giat->nama_giat}}</label>
                                        <div class="col-sm-12 col-lg-6">
                                            <select name="status_jadwals_id" class="form-control @error('status_jadwals_id') is-invalid @enderror">

                                                @foreach($statusJadwal as $data)
                                                    <option <?php if($giat->status_jadwals_id == $data->id) echo 'selected="selected"' ?> value="{{$data->id}}">{{$data->nama}}</option>
                                                @endforeach

                                            </select>

                                            @error('status_jadwals_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer text-sm">
                                    <button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>
                                    <a href="{{url('/status-aplikasi')}}">
                                        <button type="button" class="btn btn-info mr-2"><i class="fas fa-angle-left"></i> Kembali</button>
                                    </a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
@endsection