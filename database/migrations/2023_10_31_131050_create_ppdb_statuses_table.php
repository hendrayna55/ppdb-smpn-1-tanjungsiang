<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePpdbStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppdb_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('nama_agenda');
            $table->unsignedBigInteger('status_jadwals_id');
            $table->foreign('status_jadwals_id')->references('id')->on('status_jadwals')->onDelete('cascade');
            $table->timestamps();
        });

        DB::table('ppdb_statuses')->insert(
            array(
                ['nama_agenda' => 'Penerimaan Peserta Didik Baru', 'status_jadwals_id' => 1],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppdb_statuses');
    }
}
