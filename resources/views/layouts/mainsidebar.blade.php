@php
    $aplikasi = App\Models\ApplicationName::first();
@endphp

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
        <img src="{{asset('assets/dist/img/' . $aplikasi->application_logo)}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">PPDB Online</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/dist/img/logo-user.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="" class="d-block">{{Auth::user()->email}}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ url('/dashboard') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                
                @if (Auth::user()->role_id == 1)
                    <li class="nav-item">
                        <a href="{{ url('/pendaftar') }}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Pendaftar
                            </p>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->role_id == 1)
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahapan Pendaftaran
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('/tahap-1/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 1 : Biodata
                                    </p>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a href="{{ url('/tahap-2/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 2 : Alamat
                                    </p>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a href="{{ url('/tahap-3/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 3 : Asal Sekolah
                                    </p>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a href="{{ url('/tahap-4/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 4 : Data Orangtua
                                    </p>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a href="{{ url('/tahap-5/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 5 : Data Nilai
                                    </p>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a href="{{ url('/tahap-6/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 6 : Foto
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ url('/tahap-7/' . Auth::user()->id) }}" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Tahap 7 : Verifikasi
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @else
                    <li class="nav-item">
                        <a href="{{ url('/tahap-1/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 1 : Biodata
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-2/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 2 : Alamat
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-3/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 3 : Asal Sekolah
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-4/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 4 : Data Orangtua
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-5/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 5 : Data Nilai
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-6/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 6 : Pas Photo
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/tahap-7/' . Auth::user()->id) }}" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Tahap 7 : Verifikasi
                            </p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview text-sm" style="background-color: #41434D">

                        @if(Auth::user()->role_id == 1)
                            <li class="nav-item">
                                <a href="{{url('/data-aplikasi')}}" class="nav-link">
                                <i class="nav-icon fas fa-wrench"></i>
                                    <p>Data Aplikasi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/status-aplikasi')}}" class="nav-link">
                                <i class="nav-icon fas fa-wrench"></i>
                                    <p>Status Aplikasi</p>
                                </a>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a href="{{url('/akun/ubah-password')}}" class="nav-link">
                                <i class="nav-icon fas fa-lock"></i>
                                <p>Ubah Password</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{url('/contact-person')}}" class="nav-link">
                        <i class="nav-icon fas fa-headset"></i>
                        <p>
                            Contact Person
                        </p>
                    </a>
                </li>                

                <li class="nav-item">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>