@extends('layouts.app')

@section('title')
    Tahap 5
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 5 : Data Nilai</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 5 <i class="fas fa-angle-right"></i> Data Nilai
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-6 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-success">
                            <div class="card-header">
                                <h3 class="card-title">Data Nilai Matematika</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-5/nilai-matematika/' . $user->id)}}" method="post">
                                @csrf
                                @method('put')

                                <div class="card-body text-sm">
                                    <div class="row justify-content-center mx-auto align-items-center">
                                        <div class="col-lg-3 col-sm-12 text-center">
                                            <label for="kelas_4" class=""><b>Kelas 4</b></label>
                                        </div>
                                        <div class="col-lg-9 col-sm-12">
                                            <div class="form-group row">
                                                <label for="kelas4_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                <div class="col-lg-7 col-6">
                                                    <input type="number" class="form-control @error ('kelas4_1') is-invalid @enderror" id="kelas4_1" name="kelas4_1" placeholder="Contoh : 80" value="{{old('kelas4_1')?old('kelas4_1'):$user->nilaiMtk->kelas4_1}}">
        
                                                    @error('kelas4_1')
                                                        <b class="text-danger text-sm">{{$message}}</b>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kelas4_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                <div class="col-6 col-lg-7">
                                                    <input type="number" class="form-control @error ('kelas4_2') is-invalid @enderror" id="kelas4_2" name="kelas4_2" placeholder="Contoh : 80" value="{{old('kelas4_2')?old('kelas4_2'):$user->nilaiMtk->kelas4_2}}">
        
                                                    @error('kelas4_2')
                                                        <b class="text-danger text-sm">{{$message}}</b>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row justify-content-center mx-auto align-items-center">
                                        <div class="col-lg-3 col-sm-12 text-center">
                                            <label for="kelas_5" class=""><b>Kelas 5</b></label>
                                        </div>
                                        <div class="col-lg-9 col-sm-12">
                                            <div class="form-group row">
                                                <label for="kelas5_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                <div class="col-lg-7 col-6">
                                                    <input type="number" class="form-control @error ('kelas5_1') is-invalid @enderror" id="kelas5_1" name="kelas5_1" placeholder="Contoh : 80" value="{{old('kelas5_1')?old('kelas5_1'):$user->nilaiMtk->kelas5_1}}">
        
                                                    @error('kelas5_1')
                                                        <b class="text-danger text-sm">{{$message}}</b>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kelas5_2" class="col-6 col-lg-5 col-form-label">Semester 2</label>
                                                <div class="col-6 col-lg-7">
                                                    <input type="number" class="form-control @error ('kelas5_2') is-invalid @enderror" id="kelas5_2" name="kelas5_2" placeholder="Contoh : 80" value="{{old('kelas5_2')?old('kelas5_2'):$user->nilaiMtk->kelas5_2}}">
        
                                                    @error('kelas5_2')
                                                        <b class="text-danger text-sm">{{$message}}</b>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center mx-auto align-items-center">
                                        <div class="col-lg-3 col-sm-12 text-center">
                                            <label for="kelas_6" class=""><b>Kelas 6</b></label>
                                        </div>
                                        <div class="col-lg-9 col-sm-12">
                                            <div class="form-group row">
                                                <label for="kelas6_1" class="col-6 col-lg-5 col-form-label">Semester 1</label>
                                                <div class="col-lg-7 col-6">
                                                    <input type="number" class="form-control @error ('kelas6_1') is-invalid @enderror" id="kelas6_1" name="kelas6_1" placeholder="Contoh : 80" value="{{old('kelas6_1')?old('kelas6_1'):$user->nilaiMtk->kelas6_1}}">
        
                                                    @error('kelas6_1')
                                                        <b class="text-danger text-sm">{{$message}}</b>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    <button type="submit" class="btn btn-success float-right"><i class="fas fa-save"></i> Simpan</button>

                                    <a href="{{url('/tahap-5/' . $user->id)}}">
                                        <button type="button" class="btn btn-info"><i class="fas fa-backward"></i> Kembali</button>
                                    </a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection