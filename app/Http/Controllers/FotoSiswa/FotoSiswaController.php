<?php

namespace App\Http\Controllers\FotoSiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use File;
use App\Models\User;
use App\Models\BiodataSiswa;
use Auth;

class FotoSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-6.index', compact('user'));
        }else{
            abort(403);
        }
    }

    public function edit($id)
    {
        $user = User::find($id);

        if(Auth::user()->id == $user->id || Auth::user()->role_id == 1){
            return view('tahap-6.edit', compact('user'));
        }else{
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto_siswa' => 'required|mimes:jpg,jpeg,png|max:2048'
        ]);

        $user = User::find($id);

        if($request->hasFile('foto_siswa')){
            $data = BiodataSiswa::where('user_id', $user->id)->first();
            File::delete(public_path('assets/dist/img/pas-photo/'. $data->foto_siswa));

            $file = $request->file('foto_siswa');
            $nama = $user->nama_lengkap;
            $asal = $user->asalSekolah->asal_sekolah;
            $imgName = $nama . '-' . $asal . '-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('assets/dist/img/pas-photo'),$imgName);
        }

        BiodataSiswa::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'foto_siswa' => $imgName,
            'nik_siswa' => \DB::raw('nik_siswa'),
            'nisn' => \DB::raw('nisn'),
            'tempat_lahir' => \DB::raw('tempat_lahir'),
            'tanggal_lahir' => \DB::raw('tanggal_lahir'),
            'jenis_kelamin' => \DB::raw('jenis_kelamin'),
            'no_hp' => \DB::raw('no_hp'),
            'alamat' => \DB::raw('alamat'),
        ]);

        alert()->success('Berhasil', 'Update Tahap 6 : Pas Photo');
        return redirect('/tahap-6/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
