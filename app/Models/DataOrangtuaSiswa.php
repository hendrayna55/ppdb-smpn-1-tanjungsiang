<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataOrangtuaSiswa extends Model
{
    protected $table = 'data_orangtua_siswas';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
