<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiIpa extends Model
{
    protected $table = 'nilai_ipas';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
