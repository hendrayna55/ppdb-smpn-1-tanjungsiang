@extends('layouts.app')

@section('title')
    Tahap 2
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 2 : Alamat</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 2 <i class="fas fa-angle-right"></i> Alamat
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Alamat Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-2/' . $user->id)}}" method="post">
                                @csrf

                                <div class="card-body text-sm">
                                    <div class="form-group row">
                                        <label for="alamat" class="col-sm-12 col-lg-3 col-form-label">Alamat Lengkap</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->alamat == null)
                                                <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control text-danger @error ('alamat') is-invalid @enderror" placeholder="Contoh : Jln. Raya Sindanglaya No. 29" readonly>Belum Update</textarea>
                                            @else
                                                <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control @error ('alamat') is-invalid @enderror" placeholder="Contoh : Jln. Raya Sindanglaya No. 29" readonly>{{old('alamat')?old('alamat'):$user->biodata->alamat}}</textarea>
                                            @endif

                                            @error('alamat')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}

                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                        <a href="{{url('/tahap-2/edit/' . $user->id)}}">
                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                        </a>
                                    @endif
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection