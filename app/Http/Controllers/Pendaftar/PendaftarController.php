<?php

namespace App\Http\Controllers\Pendaftar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use File;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\BiodataSiswa;
use App\Models\RiwayatPendidikanSiswa;
use App\Models\DataOrangtuaSiswa;
use App\Models\NilaiBahasaIndonesia;
use App\Models\NilaiMatematika;
use App\Models\NilaiIpa;
use App\Models\VerifikasiPendaftaran;

class PendaftarController extends Controller
{
    public function index()
    {
        $data = User::where('role_id', 2)->orderBy('nama_lengkap', 'asc')->get();
        return view('pendaftar.index', compact('data'));
    }

    public function verifikasiData(Request $request, $id)
    {
        $user = User::find($id);
        VerifikasiPendaftaran::where('user_id', $user->id)->update([
            'user_id' => \DB::raw('user_id'),
            'verifikasi_pendaftaran' => $request->verifikasi_pendaftaran,
        ]);

        alert()->success('Berhasil', 'Update Verifikasi Data ' . $user->nama_lengkap);
        return redirect('/pendaftar');
    }

    public function verifikasi(Request $request, $id)
    {
        $user = User::find($id);
        User::find($id)->update([
            'nama_lengkap' => \DB::raw('nama_lengkap'),
            'role_id' => \DB::raw('role_id'),
            'email' => \DB::raw('email'),
            'status_verifikasi' => $request->status_verifikasi,
        ]);

        alert()->success('Berhasil', 'Update Verifikasi Akun ' . $user->nama_lengkap);
        return redirect('/pendaftar');
    }

    public function create()
    {
        return view('pendaftar.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => ['required', 'string', 'max:255'],
            'jenis_kelamin' => ['required'],
            'nisn' => ['required'],
            'asal_sekolah' => ['required'],
            'no_hp' => ['required'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::create([
            'nama_lengkap' => $request->nama_lengkap,
            'role_id' => 2,
            'email' => $request->email,
            'status_verifikasi' => 0,
            'password' => Hash::make($request->password),
        ]);

        BiodataSiswa::create([
            'user_id' => $user->id,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nisn' => $request->nisn,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_hp' => $request->no_hp,
        ]);

        RiwayatPendidikanSiswa::create([
            'user_id' => $user->id,
            'asal_sekolah' => $request->asal_sekolah,
        ]);

        DataOrangtuaSiswa::create([
            'user_id' => $user->id,
        ]);

        NilaiBahasaIndonesia::create([
            'user_id' => $user->id,
        ]);

        NilaiMatematika::create([
            'user_id' => $user->id,
        ]);

        NilaiIpa::create([
            'user_id' => $user->id,
        ]);

        VerifikasiPendaftaran::create([
            'user_id' => $user->id,
        ]);

        alert()->success('Berhasil', 'Tambah Pendaftar ' . $request->nama_lengkap);
        return redirect('/pendaftar');
    }

    public function edit($id)
    {
        $data = User::find($id);
        return view('pendaftar.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => ['required', 'string', 'max:255'],
            'jenis_kelamin' => ['required'],
            'nisn' => ['required'],
            'asal_sekolah' => ['required'],
            'no_hp' => ['required'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $user = User::find($id);
        User::find($id)->update([
            'nama_lengkap' => $request->nama_lengkap,
            'role_id' => 2,
            'email' => $request->email,
            'status_verifikasi' => \DB::raw('status_verifikasi')
        ]);

        BiodataSiswa::where('user_id', $user->id)->update([
            'user_id' => $user->id,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nisn' => $request->nisn,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_hp' => $request->no_hp,
        ]);

        RiwayatPendidikanSiswa::where('user_id', $user->id)->update([
            'user_id' => $user->id,
            'asal_sekolah' => $request->asal_sekolah,
        ]);

        alert()->success('Berhasil', 'Edit Pendaftar ' . $request->nama_lengkap);
        return redirect('/pendaftar');
    }

    public function destroy($id)
    {
        $data = User::find($id);
        $foto = BiodataSiswa::where('user_id', $data->id)->first();
        File::delete(public_path('assets/dist/img/pas-photo/'. $foto->foto_siswa));

        $data->delete();

        alert()->success('Berhasil', 'Hapus Pendaftar');
        return redirect('/pendaftar');
    }
}
