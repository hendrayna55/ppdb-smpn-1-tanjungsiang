<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusJadwal extends Model
{
    protected $table = 'status_jadwals';
    protected $guarded = ['id'];

    public function jadwalKta()
    {
        return $this->hasOne('App\Models\StatusJadwalKta');
    }

    public function jadwalAgenda()
    {
        return $this->hasOne('App\Models\PpdbStatus');
    }
}
