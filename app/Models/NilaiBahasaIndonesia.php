<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiBahasaIndonesia extends Model
{
    protected $table = 'nilai_bahasa_indonesias';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
