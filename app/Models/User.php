<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap', 'email', 'password', 'role_id', 'status_verifikasi'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function biodata()
    {
        return $this->hasOne('App\Models\BiodataSiswa');
    }

    public function orangtua()
    {
        return $this->hasOne('App\Models\DataOrangtuaSiswa');
    }

    public function nilaiIndo()
    {
        return $this->hasOne('App\Models\NilaiBahasaIndonesia');
    }

    public function nilaiMtk()
    {
        return $this->hasOne('App\Models\NilaiMatematika');
    }

    public function nilaiIpa()
    {
        return $this->hasOne('App\Models\NilaiIpa');
    }

    public function asalSekolah()
    {
        return $this->hasOne('App\Models\RiwayatPendidikanSiswa');
    }

    public function verifikasi()
    {
        return $this->hasOne('App\Models\VerifikasiPendaftaran');
    }
}
