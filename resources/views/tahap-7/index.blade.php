@extends('layouts.app')

@section('title')
    Tahap 7
@endsection

@push('styles')
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/toastr/toastr.min.css">
@endpush

@push('scripts')
    <!-- SweetAlert2 -->
    <script src="{{asset('assets')}}/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="{{asset('assets')}}/plugins/toastr/toastr.min.js"></script>

    <script>
        $(function() {
          var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
      
          $('.swalDefaultSuccess').click(function() {
            Toast.fire({
              icon: 'success',
              title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.swalDefaultInfo').click(function() {
            Toast.fire({
              icon: 'info',
              title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.swalDefaultError').click(function() {
            Toast.fire({
              icon: 'error',
              title: 'Belum selesai melengkapi data tahap 1 - tahap 6.'
            })
          });
          $('.swalDefaultWarning').click(function() {
            Toast.fire({
              icon: 'warning',
              title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.swalDefaultQuestion').click(function() {
            Toast.fire({
              icon: 'question',
              title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
      
          $('.toastrDefaultSuccess').click(function() {
            toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
          });
          $('.toastrDefaultInfo').click(function() {
            toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
          });
          $('.toastrDefaultError').click(function() {
            toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
          });
          $('.toastrDefaultWarning').click(function() {
            toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
          });
      
          $('.toastsDefaultDefault').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultTopLeft').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              position: 'topLeft',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultBottomRight').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              position: 'bottomRight',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultBottomLeft').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              position: 'bottomLeft',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultAutohide').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              autohide: true,
              delay: 750,
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultNotFixed').click(function() {
            $(document).Toasts('create', {
              title: 'Toast Title',
              fixed: false,
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultFull').click(function() {
            $(document).Toasts('create', {
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              icon: 'fas fa-envelope fa-lg',
            })
          });
          $('.toastsDefaultFullImage').click(function() {
            $(document).Toasts('create', {
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              image: '../../dist/img/user3-128x128.jpg',
              imageAlt: 'User Picture',
            })
          });
          $('.toastsDefaultSuccess').click(function() {
            $(document).Toasts('create', {
              class: 'bg-success',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultInfo').click(function() {
            $(document).Toasts('create', {
              class: 'bg-info',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultWarning').click(function() {
            $(document).Toasts('create', {
              class: 'bg-warning',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultDanger').click(function() {
            $(document).Toasts('create', {
              class: 'bg-danger',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
          $('.toastsDefaultMaroon').click(function() {
            $(document).Toasts('create', {
              class: 'bg-maroon',
              title: 'Toast Title',
              subtitle: 'Subtitle',
              body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
          });
        });
    </script>
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 7 : Verifikasi</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 7 <i class="fas fa-angle-right"></i> Verifikasi
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Verifikasi Data Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-7/' . $user->id)}}" method="post">
                                @csrf
                                @method('put')

                                <div class="card-body text-sm">
                                    <div class="row">
                                        <div class="col-12 col-lg-4 col-sm-12 mb-3 text-center">
                                            @if ($user->biodata->foto_siswa == null)
                                                <a href="{{url('/tahap-6/' . $user->id)}}"><img src="{{asset('assets/dist/img/pas-photo/pas-photo-kosong.jpg')}}" alt="Pas Photo" width="200px" class=""></a><br>
                                                <small><b class="text-danger text-center"><i>**Belum Update Pas Photo</i></b></small>
                                            @else
                                                <img src="{{asset('assets/dist/img/pas-photo/' . $user->biodata->foto_siswa)}}" alt="Pas Photo" width="200px">
                                            @endif
                                        </div>
        
                                        <div class="col-12 col-lg-8 col-sm-12">
                                            <label for=""><a href="{{url('/tahap-1/' . $user->id)}}">Biodata</a></label><br>
                                            <div class="row text-sm">
                                                <div class="col-4 col-lg-4 col-sm-3">
                                                    Nama Lengkap<br>
                                                    Jenis Kelamin<br>
                                                    Nomor HP<br>
                                                    Email<br>
                                                    Tmpt, Tgl Lahir<br>
                                                    NISN<br>
                                                    NIK<br>
                                                </div>
                                                <div class="col-8 col-lg-8 col-sm-9">
                                                    : {{$user->nama_lengkap}}<br>
                                                    : {{$user->biodata->jenis_kelamin}}<br>
                                                    : {{$user->biodata->no_hp}}<br>
                                                    : {{$user->email}}<br>
            
                                                    @if ($user->biodata->tempat_lahir == null || $user->biodata->tanggal_lahir == null)
                                                        : <span class="text-danger">Belum Update Data Lanjutan</span><br>
                                                    @else
                                                        : {{$user->biodata->tempat_lahir}}, {{date('d M Y', strtotime($user->biodata->tanggal_lahir))}}<br>
                                                    @endif
            
                                                    @if ($user->biodata->nisn == null)
                                                        : <span class="text-danger">Belum Update Data Lanjutan</span><br>
                                                    @else
                                                        : {{$user->biodata->nisn}}<br>
                                                    @endif
            
                                                    @if ($user->biodata->nik_siswa == null)
                                                        : <span class="text-danger">Belum Update Tahap 1</span><br><br>
                                                    @else
                                                        : {{$user->biodata->nik_siswa}}<br><br>
                                                    @endif
                                                    
                                                </div>
                                            </div>
        
                                            <label for=""><a href="{{url('/tahap-2/' . $user->id)}}">Alamat</a></label><br>
                                            <div class="row text-sm">
                                                <div class="col-12 col-lg-12 col-sm-12">
                                                    @if ($user->biodata->alamat == null)
                                                    <span class="text-danger">Belum Update Tahap 2</span><br><br>
                                                    @else
                                                    {{$user->biodata->alamat}}<br><br>
                                                    @endif
                                                </div>
                                            </div>
        
                                            <label for=""><a href="{{url('/tahap-3/' . $user->id)}}">Riwayat Pendidikan</a></label><br>
                                            <div class="row text-sm">
                                                <div class="col-4 col-lg-4 col-sm-4">
                                                    Asal Sekolah<br>
                                                    Tahun Lulus<br>
                                                </div>
                                                <div class="col-8 col-lg-8 col-sm-8">
                                                    : {{$user->asalSekolah->asal_sekolah}}<br>
            
                                                    @if ($user->asalSekolah->tahun_lulus == null)
                                                        : <span class="text-danger">Belum Update Tahap 3</span><br><br>
                                                    @else
                                                        : {{$user->asalSekolah->tahun_lulus}}<br><br>
                                                    @endif
                                                </div>
                                            </div>
        
                                            <label for=""><a href="{{url('/tahap-4/' . $user->id)}}">Data Orang Tua</a></label><br>
                                            <div class="row text-sm">
                                                <div class="col-4 col-lg-4 col-sm-5">
                                                    Nama Ayah<br>
                                                    Pekerjaan Ayah<br>
                                                    Nama Ibu<br>
                                                    Pekerjaan Ibu<br>
                                                </div>
                                                <div class="col-8 col-lg-8 col-sm-7">
                                                    @if ($user->orangtua->nama_ayah == null)
                                                    : <span class="text-danger">Belum Update Tahap 4</span><br>
                                                    @else
                                                    : {{$user->orangtua->nama_ayah}}<br>
                                                    @endif
                                                    
                                                    @if ($user->orangtua->pekerjaan_ayah == null)
                                                    : <span class="text-danger">Belum Update Tahap 4</span><br>
                                                    @else
                                                    : {{$user->orangtua->pekerjaan_ayah}}<br>
                                                    @endif
            
                                                    @if ($user->orangtua->nama_ibu == null)
                                                    : <span class="text-danger">Belum Update Tahap 4</span><br>
                                                    @else
                                                    : {{$user->orangtua->nama_ibu}}<br>
                                                    @endif
                                                    
                                                    @if ($user->orangtua->pekerjaan_ibu == null)
                                                    : <span class="text-danger">Belum Update Tahap 4</span><br><br>
                                                    @else
                                                    : {{$user->orangtua->pekerjaan_ibu}}<br><br>
                                                    @endif
                                                </div>
                                            </div>
        
                                            <label for=""><a href="{{url('/tahap-5/' . $user->id)}}">Data Rata-Rata Nilai</a></label><br>
                                            <div class="row text-sm">
                                                <div class="col-4 col-lg-4 col-sm-5">
                                                    Bahasa Indonesia<br>
                                                    Matematika<br>
                                                    IPA<br>
                                                </div>
                                                <div class="col-8 col-lg-8 col-sm-7">
                                                    @php
                                                        $avgIndo = ($user->nilaiIndo->kelas4_1 + $user->nilaiIndo->kelas4_2 + $user->nilaiIndo->kelas5_1 + $user->nilaiIndo->kelas5_2 + $user->nilaiIndo->kelas6_1)/5;
            
                                                        $avgMtk = ($user->nilaiMtk->kelas4_1 + $user->nilaiMtk->kelas4_2 + $user->nilaiMtk->kelas5_1 + $user->nilaiMtk->kelas5_2 + $user->nilaiMtk->kelas6_1)/5;
            
                                                        $avgIpa = ($user->nilaiIpa->kelas4_1 + $user->nilaiIpa->kelas4_2 + $user->nilaiIpa->kelas5_1 + $user->nilaiIpa->kelas5_2 + $user->nilaiIpa->kelas6_1)/5;
                                                    @endphp
            
                                                    : <b>{{$avgIndo}}</b><br>
                                                    
                                                    : <b>{{$avgMtk}}</b><br>
            
                                                    : <b>{{$avgIpa}}</b><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}

                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0)
                                        @if (($user->biodata->foto_siswa == null) || ($user->biodata->nik_siswa == null) || ($user->biodata->alamat == null) || ($user->orangtua->nama_ayah == null) || ($user->orangtua->pekerjaan_ayah == null) || ($user->orangtua->nama_ibu == null) || ($user->orangtua->pekerjaan_ibu == null) || ($user->nilaiIndo->kelas4_1 == 0) || ($user->nilaiIndo->kelas4_2 == 0) || ($user->nilaiIndo->kelas5_1 == 0) || ($user->nilaiIndo->kelas5_2 == 0) || ($user->nilaiIndo->kelas6_1 == 0) || ($user->nilaiMtk->kelas4_1 == 0) || ($user->nilaiMtk->kelas4_2 == 0) || ($user->nilaiMtk->kelas5_1 == 0) || ($user->nilaiMtk->kelas5_2 == 0) || ($user->nilaiMtk->kelas6_1 == 0) || ($user->nilaiIpa->kelas4_1 == 0) || ($user->nilaiIpa->kelas4_2 == 0) || ($user->nilaiIpa->kelas5_1 == 0) || ($user->nilaiIpa->kelas5_2 == 0) || ($user->nilaiIpa->kelas6_1 == 0) || ($user->asalSekolah->tahun_lulus == null))
                                            <button type="button" class="btn btn-success float-right swalDefaultError">
                                                <i class="fas fa-check"></i> Verifikasi
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#modal-verifikasi-data{{$user->id}}"><i class="fas fa-check"></i> Verifikasi</button>
                                        @endif
                                    @endif

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->

                        <!-- Modal Verifikasi Data -->
                        <div class="modal fade" id="modal-verifikasi-data{{$user->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Verifikasi Data</h4>
                                        <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button> -->
                                    </div>
                                    <div class="modal-body">
                                        <p>Saat memverifikasi data pendaftar PPDB SMPN 1 Tanjungsiang, anda tidak dapat merubah lagi data yang sudah diinput.</p>
                                        <p>Apakah anda yakin data yang diinput sudah sesuai dan verifikasi?</p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <form action="{{url('/tahap-7/' . $user->id)}}" method="post" class="">
                                            @csrf
                                            @method('put')
                                            <button type="submit" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#modal-hapus">Verifikasi</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection