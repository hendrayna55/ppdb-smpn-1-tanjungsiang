<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusJadwal;
use App\Models\GiatGudep;
use App\Models\PpdbStatus;
use Alert;

class StatusAplikasiController extends Controller
{
    public function index()
    {
        $giat = PpdbStatus::all();
        return view('application-status.index', compact('giat'));
    }

    public function edit($id)
    {
        $statusJadwal = StatusJadwal::all();
        $giat = PpdbStatus::find($id);
        return view('application-status.edit', compact('statusJadwal', 'giat'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'status_jadwals_id' => 'required'
        ]);

        PpdbStatus::find($id)->update([
            'status_jadwals_id' => $request->status_jadwals_id
        ]);

        alert()->success('Berhasil', 'Update Status Kegiatan');
        return redirect('/status-aplikasi');
    }
}
