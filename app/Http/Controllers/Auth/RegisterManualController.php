<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\BiodataSiswa;
use App\Models\DataOrangtuaSiswa;
use App\Models\NilaiBahasaIndonesia;
use App\Models\NilaiMatematika;
use App\Models\NilaiIpa;
use App\Models\RiwayatPendidikanSiswa;
use App\Models\VerifikasiPendaftaran;
use App\Models\PpdbStatus;
use Alert;
use Auth;

class RegisterManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = PpdbStatus::first();

        if ($status->status_jadwals_id == 1) {
            return view('auth.register');
        }else{
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => ['required'],
            'jenis_kelamin' => ['required'],
            'asal_sekolah' => ['required'],
            'no_hp' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
        ]);

        $user = User::create([
            'nama_lengkap' => $request->nama_lengkap,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        BiodataSiswa::create([
            'user_id' => $user->id,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp' => $request->no_hp,
        ]);

        RiwayatPendidikanSiswa::create([
            'user_id' => $user->id,
            'asal_sekolah' => $request->asal_sekolah,
        ]);

        DataOrangtuaSiswa::create([
            'user_id' => $user->id,
        ]);

        NilaiBahasaIndonesia::create([
            'user_id' => $user->id,
        ]);

        NilaiMatematika::create([
            'user_id' => $user->id,
        ]);

        NilaiIpa::create([
            'user_id' => $user->id,
        ]);

        VerifikasiPendaftaran::create([
            'user_id' => $user->id,
        ]);

        Auth::logout();
        alert()->success('Berhasil', 'Membuat Akun Baru ' . $request->nama_lengkap);
        return redirect('/login');
    }

    public function tahap2($id)
    {
        $user = User::find($id);
        return view('auth.lengkapiData', compact('user'));
    }

    public function tahapData2(Request $request, $id)
    {
        $request->validate([
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'nisn' => ['required'],
        ]);

        $user = User::find($id);
        BiodataSiswa::where('user_id', $user->id)->update([
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'nisn' => $request->nisn,
        ]);

        alert()->success('Berhasil', 'Lengkapi Data');
        return redirect('/dashboard');
    }

    public function logout(Request $request) {
        Auth::logout();
        alert()->success('Berhasil','Logout');
        return redirect('/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
