@extends('layouts.app')

@section('title')
    Tahap 1
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content / Konten Utama -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-6 col-sm-12">
                        <h1>Tahap 1 : Biodata</h1>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <!-- <li class="breadcrumb-item active">
                                <i class="fas fa-building"></i> Pangkalan
                            </li> -->
                            <li class="breadcrumb-item active">
                                <i class="fas fa-file"></i> Tahap 1 <i class="fas fa-angle-right"></i> Biodata
                            </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!-- left column -->
                    <div class="col-lg-8 col-sm-12">
                        <!-- general form elements -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Biodata Pendaftar PPDB</h3>
                            </div>
                            <!-- /.card-header -->

                            <!-- form start -->
                            <form class="form-horizontal text-sm" action="{{url('/tahap-1/' . $user->id)}}" method="post">
                                @csrf

                                <div class="card-body text-sm">
                                    <div class="form-group row">
                                        <label for="nama_lengkap" class="col-sm-12 col-lg-3 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('nama_lengkap') is-invalid @enderror" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama" value="{{old('nama_lengkap')?old('nama_lengkap'):$user->nama_lengkap}}" readonly>

                                            @error('nama_lengkap')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="jenis_kelamin" class="col-sm-12 col-lg-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-12 col-lg-8">
                                            <input type="text" class="form-control @error ('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin" name="jenis_kelamin" placeholder="Masukkan Nama" value="{{old('jenis_kelamin')?old('jenis_kelamin'):$user->biodata->jenis_kelamin}}" readonly>

                                            @error('jenis_kelamin')
                                                <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nik_siswa" class="col-sm-12 col-lg-3 col-form-label">NIK</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->nik_siswa == null)
                                                <input type="text" class="text-danger form-control @error ('nik_siswa') is-invalid @enderror" id="nik_siswa" name="nik_siswa" placeholder="Masukkan NIK" value="Belum Update" readonly>
                                            @else
                                                <input type="number" class="form-control @error ('nik_siswa') is-invalid @enderror" id="nik_siswa" name="nik_siswa" placeholder="Masukkan NIK" value="{{old('nik_siswa')?old('nik_siswa'):$user->biodata->nik_siswa}}" readonly>
                                            @endif

                                            @error('nik_siswa')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nisn" class="col-sm-12 col-lg-3 col-form-label">NISN</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->nisn == null)
                                                <input type="text" class="text-danger form-control @error ('nisn') is-invalid @enderror" id="nisn" name="nisn" placeholder="Masukkan NIK" value="Belum Update" readonly>
                                            @else
                                                <input type="number" class="form-control @error ('nisn') is-invalid @enderror" id="nisn" name="nisn" placeholder="Masukkan NIK" value="{{old('nisn')?old('nisn'):$user->biodata->nisn}}" readonly>
                                            @endif

                                            @error('nisn')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tempat_lahir" class="col-sm-12 col-lg-3 col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->tempat_lahir == null)
                                                <input type="text" class="text-danger form-control @error ('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="Belum Update" readonly>
                                            @else
                                                <input type="text" class="form-control @error ('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="{{old('tempat_lahir')?old('tempat_lahir'):$user->biodata->tempat_lahir}}" readonly>
                                            @endif

                                            @error('tempat_lahir')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tanggal_lahir" class="col-sm-12 col-lg-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-12 col-lg-8">
                                            @if ($user->biodata->tanggal_lahir == null)
                                                <input type="text" class="text-danger form-control @error ('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir" value="Belum Update" readonly>
                                            @else
                                                <input type="date" class="form-control @error ('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir" value="{{old('tanggal_lahir')?old('tanggal_lahir'):$user->biodata->tanggal_lahir}}" readonly>
                                            @endif

                                            @error('tanggal_lahir')
                                                <b class="text-danger text-sm">{{$message}}</b>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer text-sm">
                                    
                                    {{-- <button type="submit" class="btn btn-success float-right"><i class="fas fa-edit"></i> Edit</button> --}}

                                    @if ($user->verifikasi->verifikasi_pendaftaran == 0 || Auth::user()->role_id == 1)
                                        <a href="{{url('/tahap-1/edit/' . $user->id)}}">
                                            <button type="button" class="btn btn-info float-right"><i class="fas fa-edit"></i> Edit</button>
                                        </a>
                                    @endif
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection