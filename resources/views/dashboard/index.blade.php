@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@push('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": false, "lengthChange": false, "autoWidth": false, "ordering": true,
                "buttons": ["csv", "excel", "pdf", "print",] , "scrollX" : true
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": false,
                "scrollX" : true,
            });
        });
    </script>
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @if (Auth::user()->role_id == 1)
                    <!-- Info boxes -->
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Pendaftar</span>
                                    <span class="info-box-number">
                                        {{$totalPendaftar}} Orang
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-mars"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pendaftar Putra</span>
                                    <span class="info-box-number">{{$pendaftarPutra}} Orang</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon elevation-1" style="background-color: pink"><i class="fas fa-venus"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pendaftar Putri</span>
                                    <span class="info-box-number">{{$pendaftarPutri}} Orang</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user-check"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Menunggu Verifikasi Admin</span>
                                    <span class="info-box-number">
                                        {{$waitingVerif}} Akun
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row justify-content-center">
                        
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check"></i></span>
    
                                <div class="info-box-content">
                                    <span class="info-box-text">Sudah Verifikasi Data</span>
                                    <span class="info-box-number">{{$verifDataDone}} Orang</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
    
                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>
    
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-exclamation-circle"></i></span>
    
                                <div class="info-box-content">
                                    <span class="info-box-text">Belum Verifikasi Data</span>
                                    <span class="info-box-number">{{$verifDataUndone}} Orang</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                @endif
                

                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-8">
                        @if (Auth::user()->role_id == 1)
                            <!-- Table Progres Pendaftaran -->
                            <div class="card">
                                <div class="card-header bg-info border-transparent">
                                    <h3 class="card-title">Progres Kelengkapan Data Pendaftar</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool text-white" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-sm nowrap" id="example2">
                                            <thead class="bg-info">
                                                <tr>
                                                    <th class="text-center align-middle">Nama Lengkap</th>
                                                    <th class="text-center align-middle">Tahap 1</th>
                                                    <th class="text-center align-middle">Tahap 2</th>
                                                    <th class="text-center align-middle">Tahap 3</th>
                                                    <th class="text-center align-middle">Tahap 4</th>
                                                    <th class="text-center align-middle">Tahap 5</th>
                                                    <th class="text-center align-middle">Tahap 6</th>
                                                    <th class="text-center align-middle">Verifikasi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($allUser as $item)
                                                    <tr>
                                                        <td><a href="{{url('/tahap-7/' . $item->id)}}">{{$item->nama_lengkap}}</a></td>
                                                        @if ($item->biodata->nik_siswa == null)
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if ($item->biodata->alamat == null)
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if ($item->asalSekolah->tahun_lulus == null)
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if (($item->orangtua->nama_ayah == null) || ($item->orangtua->pekerjaan_ayah == null) || ($item->orangtua->nama_ibu == null) || ($item->orangtua->pekerjaan_ibu == null))
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if (($item->nilaiIndo->kelas4_1 == 0) || ($item->nilaiIndo->kelas4_2 == 0) || ($item->nilaiIndo->kelas5_1 == 0) || ($item->nilaiIndo->kelas5_2 == 0) || ($item->nilaiIndo->kelas6_1 == 0) || ($item->nilaiMtk->kelas4_1 == 0) || ($item->nilaiMtk->kelas4_2 == 0) || ($item->nilaiMtk->kelas5_1 == 0) || ($item->nilaiMtk->kelas5_2 == 0) || ($item->nilaiMtk->kelas6_1 == 0) || ($item->nilaiIpa->kelas4_1 == 0) || ($item->nilaiIpa->kelas4_2 == 0) || ($item->nilaiIpa->kelas5_1 == 0) || ($item->nilaiIpa->kelas5_2 == 0) || ($item->nilaiIpa->kelas6_1 == 0))
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if ($item->biodata->foto_siswa == null)
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif

                                                        @if ($item->verifikasi->verifikasi_pendaftaran == 0)
                                                            <td class="text-danger text-center align-middle"><i class="fas fa-times-circle"></i></td>
                                                        @else
                                                            <td class="text-success text-center align-middle"><i class="fas fa-check-circle"></i></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    {{-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                                    <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> --}}
                                </div>
                                <!-- /.card-footer -->
                            </div>
                            <!-- /.card -->
                        @endif

                        <!-- Papan Informasi -->
                        <div class="card">
                            <div class="card-header bg-info border-transparent">
                                <h3 class="card-title">Informasi</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool text-white" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="text-center mb-3">
                                    <img src="{{asset('assets/dist/img/' . $informasi->foto_informasi)}}" alt="Gambar Informasi" width="500px" class="rounded mx-auto d-block img-fluid">
                                </div>
                                <h5>Selamat Datang, {{Auth::user()->nama_lengkap}}</h5>
                                {!! $informasi->deskripsi_informasi !!}
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer clearfix">
                                @if (Auth::user()->role_id == 1)
                                    <a href="{{url('/')}}" class="btn btn-sm btn-success float-right"><i class="fas fa-edit"></i> Edit</a>
                                @endif
                            </div>
                            <!-- /.card-footer -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-4">
                        @if (Auth::user()->role_id == 1)
                            <!-- USERS LIST -->
                            <div class="card">
                                <div class="card-header bg-info">
                                    <h3 class="card-title">Pendaftar Baru</h3>

                                    <div class="card-tools">
                                        {{-- <span class="badge badge-danger">8 New Members</span> --}}
                                        <button type="button" class="btn btn-tool text-white" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        {{-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                                            <i class="fas fa-times"></i>
                                        </button> --}}
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        @foreach ($newUser as $item)
                                            <div class="col-3 col-lg-3 col-sm-3">
                                                @if ($item->biodata->foto_siswa == null)
                                                        <img src="{{asset('assets')}}/dist/img/pas-photo/pas-photo-kosong.jpg" alt="User Image" width="80px" height="80px" class="rounded-circle">
                                                @else
                                                    <img src="{{asset('assets/dist/img/pas-photo/' . $item->biodata->foto_siswa)}}" alt="User Image" width="80px" height="80px" class="rounded-circle">
                                                @endif
                                                <a class="users-list-name" href="{{url('/tahap-7/' . $item->id)}}">{{$item->nama_lengkap}}</a>

                                                @php
                                                    $created = date('d M y', strtotime($item->created_at));
                                                @endphp

                                                @if ($created == $today)
                                                    <span class="users-list-date">Today</span>
                                                @else
                                                    <span class="users-list-date">{{date('d M', strtotime($item->created_at))}}</span>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                    <!-- /.users-list -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer text-center">
                                <a href="{{url('/pendaftar')}}">View All Users</a>
                                </div>
                                <!-- /.card-footer -->
                            </div>
                            <!--/.card -->
                        @endif

                        <!-- Progres User -->
                        <div class="card">
                            <div class="card-header bg-info">
                                <h3 class="card-title">Progres Kelengkapan Data</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool text-white" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <ul class="products-list product-list-in-card pl-2 pr-2">
                                    <li class="item">
                                        <div class="product-img">
                                            @if (Auth::user()->biodata->nik_siswa == null)
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-1/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 1
                                                {{-- <span class="badge badge-warning float-right">$1800</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update Nomor Induk Kependudukan (NIK).
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->

                                    <li class="item">
                                        <div class="product-img">
                                            @if (Auth::user()->biodata->alamat == null)
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-2/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 2
                                                {{-- <span class="badge badge-info float-right">$700</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update Alamat.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->

                                    <li class="item">
                                        <div class="product-img">
                                            @if (Auth::user()->asalSekolah->tahun_lulus == null)
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-3/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 3
                                                {{-- <span class="badge badge-danger float-right">$350</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update tahun lulus sekolah asal.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                    
                                    <li class="item">
                                        <div class="product-img">
                                            @if ((Auth::user()->orangtua->nama_ayah == null) || (Auth::user()->orangtua->pekerjaan_ayah == null) || (Auth::user()->orangtua->nama_ibu == null) || (Auth::user()->orangtua->pekerjaan_ibu == null))
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-4/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 4
                                                {{-- <span class="badge badge-success float-right">$399</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update nama dan pekerjaan orangtua.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->

                                    <li class="item">
                                        <div class="product-img">
                                            @if ((Auth::user()->nilaiIndo->kelas4_1 == 0) || (Auth::user()->nilaiIndo->kelas4_2 == 0) || (Auth::user()->nilaiIndo->kelas5_1 == 0) || (Auth::user()->nilaiIndo->kelas5_2 == 0) || (Auth::user()->nilaiIndo->kelas6_1 == 0) || (Auth::user()->nilaiMtk->kelas4_1 == 0) || (Auth::user()->nilaiMtk->kelas4_2 == 0) || (Auth::user()->nilaiMtk->kelas5_1 == 0) || (Auth::user()->nilaiMtk->kelas5_2 == 0) || (Auth::user()->nilaiMtk->kelas6_1 == 0) || (Auth::user()->nilaiIpa->kelas4_1 == 0) || (Auth::user()->nilaiIpa->kelas4_2 == 0) || (Auth::user()->nilaiIpa->kelas5_1 == 0) || (Auth::user()->nilaiIpa->kelas5_2 == 0) || (Auth::user()->nilaiIpa->kelas6_1 == 0))
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-5/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 5
                                                {{-- <span class="badge badge-success float-right">$399</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update nilai Bahasa Indonesia, Matematika dan IPA.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->

                                    <li class="item">
                                        <div class="product-img">
                                            @if (Auth::user()->biodata->foto_siswa == null)
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-6/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 6
                                                {{-- <span class="badge badge-warning float-right">$1800</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Update pas photo.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->

                                    <li class="item">
                                        <div class="product-img">
                                            @if (Auth::user()->verifikasi->verifikasi_pendaftaran == 0)
                                                <img src="{{asset('assets')}}/dist/img/redcross.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @else
                                                <img src="{{asset('assets')}}/dist/img/checklist.png" alt="Product Image" class="img-size-50 rounded-circle">
                                            @endif
                                        </div>
                                        <div class="product-info">
                                            <a href="{{url('/tahap-7/' . Auth::user()->id)}}" class="product-title">
                                                Tahap 7 : Verifikasi
                                                {{-- <span class="badge badge-warning float-right">$1800</span> --}}
                                            </a>
                                            <span class="product-description">
                                                Verifikasi data setelah tahap 1 - tahap 6 selesai.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                </ul>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer text-center">
                                {{-- <a href="javascript:void(0)" class="uppercase">View All Products</a> --}}
                            </div>
                            <!-- /.card-footer -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection