<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Alert;

class CheckUserVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user)
            if($user->status_verifikasi == true){
                return $next($request);
            }else{
                if($user->biodata->tempat_lahir == null){
                    return redirect('/registrasi/kelengkapan-data/' . Auth::user()->id);
                }else{
                    
                    abort(401)->alert()->success('Mohon Menunggu', 'Verifikasi Admin');
                }
            }
        
        
        return abort(403);
    }
}
